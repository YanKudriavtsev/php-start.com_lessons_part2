<?php

class Router
{
    private $routes;

    public function __construct()
    {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include($routesPath);
    }

    /*
    *Returns request string
    *@return string
    */
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run()
    {
        //get request string
        $uri = $this->getURI();

        //check request in routes.php
        foreach($this->routes as $uriPattern => $path) {
            if (preg_match("~$uriPattern~", $uri)) {
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                $segments = explode('/', $internalRoute);

                // getting controller name from routes.php
                $controllerName = array_shift($segments).'Controller';
                $controllerName = ucfirst($controllerName);

                // getting action name from routes.php
                $actionName = 'action'.ucfirst(array_shift($segments));
                $parameters = $segments;
                $controllerFile = ROOT.'/controllers/'.$controllerName.'.php';
                if (file_exists($controllerFile)) {
                    include_once $controllerFile;
                }
                $controllerObject = new $controllerName;
                $result = $controllerObject->$actionName($parameters);
                if ($result !== null) {
                    break;
                }
            }
        }
    }
}