<?php

class CatalogController
{
    public function actionIndex()
    {
        $categories = Category::getCategoryList();
        $latestProducts = Product::getLatestProduct(12);
        require_once ROOT.'/views/catalog/index.php';
        return true;
    }

    public function actionCategory($params)
    {
        $categoryId = $params[0];
        if (isset($params[1])) {
            $page = $params[1];
        } else {
            $page = 1;
        }
        $categories = Category::getCategoryList();
        $categoryProducts = Product::getProductsListByCategory($categoryId, $page);
        $total = Product::getTotalProductsInCategory($categoryId);

        // paginator
        $pagination = new Pagination($total, $page, Product::SHOW_BY_DEFAULT, 'page-');
        require_once ROOT.'/views/catalog/category.php';
        return true;
    }
}