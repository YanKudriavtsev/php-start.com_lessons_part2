<?php

class CartController
{

    public function actionAdd($params)
    {
        $id = $params[0];
        Cart::addProduct($id);

        $referrer = $_SERVER['HTTP_REFERER'];
        header("Location: {$referrer}");
    }

    public function actionAddAjax($params)
    {
        $id = $params[0];
        echo '('.Cart::addProduct($id).')';
        return true;
    }

    public function actionIndex()
    {
        $categories = [];
        $category = Category::getCategoryList();

        $productsInCart = Cart::getProducts();

        if ($productsInCart) {
            $productsIds = array_keys($productsInCart);
            $products = Product::getProductByIds($productsIds);

            $totalPrice = Cart::getTotalPrice($products);
        }

        require_once ROOT . '/views/cart/index.php';
        return true;
    }
}