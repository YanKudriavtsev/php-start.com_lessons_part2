<?php
class UserController {

    public function actionRegister()
    {
        $name = null;
        $email = null;
        $password = null;
        $result = false;

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;

            if (!User::checkName($name)) {
                $errors[] = 'Невірний формат імені';
            }

            if (!User::checkPassword($password)) {
                $errors[] = 'Невірий формат паролю';
            }

            if (!User::checkEmail($email)) {
                $errors[] = 'Невірний формат email';
            }

            if (User::checkEmailExists($email)) {
                $errors[] = 'Користувач з таким email вже існує';
            }

            if ($errors == false) {
                $result = User::register($name, $email, $password);
            }

        }
        require_once ROOT.'/views/user/register.php';
        return true;
    }

    public function actionLogin()
    {
        $email = null;
        $password = null;

        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;

            if (!User::checkPassword($password)) {
                $errors[] = 'Невірий формат паролю';
            }

            if (!User::checkEmail($email)) {
                $errors[] = 'Невірний формат email';
            }

            $userId = User::checkUserData($email, $password);

            if ($userId == false) {
                $errors[] = 'Невірні дані для входу';
            } else {
                User::auth($userId);
                header('Location: /cabinet');
            }

        }
        require_once ROOT.'/views/user/login.php';
        return true;
    }

    public function actionLogout()
    {
        session_start();
        unset($_SESSION['user']);
        header('Location: /');
    }
}