<?php

class SiteController
{
    public function actionIndex()
    {
        $categories = Category::getCategoryList();
        $latestProducts = Product::getLatestProduct(3);
        require_once ROOT.'/views/site/index.php';
        return true;
    }

    public function actionContact()
    {
        $userEmail = null;
        $userText = null;
        $result = false;

        if (isset($_POST['submit'])) {
            $userEmail = $_POST['userEmail'];
            $userText = $_POST['userText'];

            $errors = false;

            if (!User::checkEmail($userEmail)) {
                $errors[] = 'Невірний формати пошти.';
            }

            if ($errors == false) {
                $adminEmail = 'yan.kudriavtsev@yahoo.com';
                $message = "Текст: {$userText} від {$userEmail}";
                $subject = 'Тема листа';
                $result = mail($adminEmail, $subject, $message);
            }
        }

        require_once ROOT . '/views/site/contacts.php';

        return true;
    }
}
