<?php

class ProductController
{
    public function actionView($params)
    {
        $productId = $params[0];
        $categories = Category::getCategoryList();
        $product = Product::getProductById($productId);
        require_once ROOT.'/views/product/view.php';
        return true;
    }
}