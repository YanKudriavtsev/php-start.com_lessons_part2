<?php include ROOT . '/views/layouts/header.php';?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 padding-right">
                <?php if($result):?>
                    <p>Повідомлення надіслане</p>
                <?php else :?>
                    <?php if(isset($errors)):?>
                        <ul>
                            <?php foreach ($errors as $error):?>
                                <li> - <?php echo $error;?></li>
                            <?php endforeach;?>
                        </ul>
                    <?php endif;?>

                    <div class="col-sm-12 signup-form">
                        <h2>Зворотній зв'язок</h2>
                        <form action="#" method="POST">
                            <input type="email" placeholder="Ваша пошта" name="userEmail" value="<?=$userEmail;?>" class="form-control">
                            <textarea name="userText" class="form-control"><?=$userText;?></textarea>
                            <br>
                            <button name="submit">Відправити</button>
                        </form>
                        <br>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>
<?php include ROOT . '/views/layouts/footer.php';?>
