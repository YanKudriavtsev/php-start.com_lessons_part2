<?php include ROOT.'/views/layouts/header.php';?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Каталог</h2>
                    <div class="panel-group category-products">
                        <?php foreach($categories as $categoryItem) :?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="/category/<?php echo $categoryItem['id']; ?>">
                                            <?php echo $categoryItem['name']; ?>
                                        </a>
                                    </h4>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>

            <?php if ($productsInCart) :?>
                <p>Ви обрали такі товари:</p>
                <table class="table-bordered table-striped table">
                    <tr>
                        <th>Код товару</th>
                        <th>Назва</th>
                        <th>Вартість, $</th>
                        <th>Кількість, шт</th>
                    </tr>
                    <?php foreach ($products as $product): ?>
                        <tr>
                            <td><?=$product['code'];?></td>
                            <td>
                                <a href="/product/<?=$product['id'];?>">
                                    <?=$product['name'];?>
                                </a>
                            </td>
                            <td><?=$product['price'];?></td>
                            <td><?=$productsInCart[$product['id']];?></td>
                        </tr>
                    <?php endforeach;?>
                        <tr>
                            <td>Загальна вартість:</td>
                            <td><?=$totalPrice;?></td>
                        </tr>
                </table>
            <?php else: ?>
                <p>Корзина пуста</p>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php include ROOT.'/views/layouts/footer.php';?>
