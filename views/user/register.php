<?php include ROOT.'/views/layouts/header.php';?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 padding-right">
                <?php if ($result):?>
                    <p>Ви зареєстровані</p>
                <?php else:?>
                    <?php if (isset($errors) && is_array($errors)):?>
                        <ul>
                            <?php foreach($errors as $error):?>
                                <li><?=$error?></li>
                            <?php endforeach;?>
                        </ul>
                    <?php endif;?>

                    <div class="signup-form">
                        <h2>Реєстрація на сайті</h2>
                        <form action="#" method="post">
                            <input type="text" name="name" placeholder="Ім'я" value="<?=$name?>">
                            <input type="email" name="email" placeholder="Email" value="<?=$email?>">
                            <input type="password" name="password" placeholder="Пароль" value="<?=$password?>">
                            <button type="submit" name="submit">Зареєструватися</button>
                        </form>
                        <br>
                        <br>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>
<?php include ROOT.'/views/layouts/footer.php';?>
