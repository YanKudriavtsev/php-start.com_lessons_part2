<?php include ROOT.'/views/layouts/header.php';?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 padding-right">
                <?php if (isset($errors) && is_array($errors)):?>
                    <ul>
                        <?php foreach($errors as $error):?>
                            <li><?=$error?></li>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>

                <div class="signup-form">
                    <h2>Вхід на сайт</h2>
                    <form action="#" method="post">
                        <input type="email" name="email" placeholder="Email" value="<?=$email?>">
                        <input type="password" name="password" placeholder="Пароль" value="<?=$password?>">
                        <button type="submit" name="submit">Вхід</button>
                    </form>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include ROOT.'/views/layouts/footer.php';?>
