<?php
class User
{

    public static function checkName($name)
    {
        if (strlen($name) >= 2) {
            return true;
        }
        return false;
    }

    public static function checkPassword($password)
    {
        if (strlen($password) >= 6) {
            return true;
        }
        return false;
    }

    public static function checkEmail($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    public static function checkEmailExists($email)
    {
        $db = Db::getConnection();
        $sql = "SELECT COUNT(*) FROM user WHERE email=?";
        $result = $db->prepare($sql);
        $result->execute([$email]);
        if($result->fetchColumn()) {
            return true;
        }
        return false;
    }

    public static function register($name, $email, $password)
    {
        $db = Db::getConnection();
        $sql = "INSERT INTO user (name, email, password, role) VALUES (?, ?, ?, ?)";
        $req = $db->prepare($sql);
        $result = $req->execute([$name, $email, $password, 'admin']);
        return $result;
    }

    public static function checkUserData($email, $password)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM user WHERE email=? AND password=?';

        $req = $db->prepare($sql);
        $req->execute([$email, $password]);

        $user = $req->fetch();
        var_dump($user);

        if ($user) {
            return $user['id'];
        }

        return false;
    }

    public static function auth($userId)
    {
        $_SESSION['user'] = $userId;
    }

    public static function checkLogged()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
    }

    public static function isGuest()
    {
        if (isset($_SESSION['user'])) {
            return false;
        }
        return true;
    }

    public static function getUserById($userId)
    {
        $db = Db::getConnection();
        $sql = "SELECT * FROM user WHERE id=?";
        $req = $db->prepare($sql);
        $req->execute([$userId]);
        return $req->fetch(PDO::FETCH_ASSOC);
    }

    public static function edit($userId, $name, $password)
    {
        $db = Db::getConnection();
        $sql = "UPDATE user SET name=?, password=? WHERE id=?";
        $req = $db->prepare($sql);
        $result = $req->execute([$name, $password, $userId]);
        return $result;
    }
}