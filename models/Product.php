<?php

class Product
{
    const SHOW_BY_DEFAULT = 3;
    public static function getLatestProduct($count = self::SHOW_BY_DEFAULT)
    {
        $db = DB::getConnection();

        $count = intval($count);
        $result = $db->query("SELECT id, name, price, is_new FROM product ".
        "WHERE status='1' ".
        "ORDER BY id DESC ".
        "LIMIT ".$count);

        $i = 0;
        while ($row = $result->fetch()) {
            $productList[$i]['id'] = $row['id'];
            $productList[$i]['name'] = $row['name'];
//            $productList[$i]['image'] = $row['image'];
            $productList[$i]['price'] = $row['price'];
            $productList[$i]['is_new'] = $row['is_new'];
            ++$i;
        }

        return $productList;
    }

    public static function getProductsListByCategory($categoryId, $page)
    {
        $db = DB::getConnection();
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        $result = $db->prepare("SELECT id, name, price, is_new FROM product ".
        "WHERE status='1' AND category_id=?".
        "ORDER BY id DESC ".
        "LIMIT ".self::SHOW_BY_DEFAULT.
        " OFFSET ".$offset);
        $result->execute([$categoryId]);

        $i = 0;
        while ($row = $result->fetch()) {
            $productList[$i]['id'] = $row['id'];
            $productList[$i]['name'] = $row['name'];
//            $productList[$i]['image'] = $row['image'];
            $productList[$i]['price'] = $row['price'];
            $productList[$i]['is_new'] = $row['is_new'];
            ++$i;
        }

        return $productList;
    }

    public static function getProductById($id)
    {
        $db = DB::getConnection();
        $req = $db->prepare("SELECT * FROM product WHERE id=?");
        $req->execute([$id]);
        $result = $req->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function getTotalProductsInCategory($categoryId)
    {
        $db = DB::getConnection();

        $rec = $db->prepare("SELECT count(id) AS count FROM product ".
        "WHERE category_id=? AND status = 1");
        $rec->execute([$categoryId]);
        $result = $rec->fetch(PDO::FETCH_ASSOC);
        return $result['count'];
    }

    public static function getProductByIds($idsArray)
    {
        $products = [];
        $db = Db::getConnection();
        $idsString = implode(', ', $idsArray);
        $sql = "SELECT * FROM product WHERE status='1' AND id IN ({$idsString})";
        $result = $db->query($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['code'] = $row['code'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            ++$i;
        }
        return $products;
    }
}