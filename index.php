<?php

//display errors for debug
ini_set('display_errors', 1);
error_reporting(E_ALL);

// components for project
define('ROOT', dirname(__FILE__));

session_start();

//autoload
require ROOT.'/components/Autoload.php';

$router = new Router();
$router->run();